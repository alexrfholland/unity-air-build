﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts {
    public class VoxelGrid
    {
        public Cell[] vals;      // The array of bytes containing the pixels.
        public int w, h, d;
        float[] min;
        float[] max;

        public VoxelGrid(int _w, int _h, int _d, float[] _min, float[] _max)
        {
            w = _w;
            h = _h;
            d = _d;
            min = _min;
            max = _max;
            initGrid();
        }

        //Initialisation Functions

        public void initGrid()
        {
            vals = new Cell[w * h * d];
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    for (int k = 0; k < d; k++)
                    {
                        int index = i + w * (j + h * k);
                        vals[index] = new Cell(0, i, j, k);
                    }
                }
            }
        }

		public void loadVobject(VObject loadedVobject) 
		{

			for (int ii = 0; ii < loadedVobject.geometry.Length; ii++) {
				Vector3 position = loadedVobject.geometry [ii].localOrigin;
				position = position;
				Vector3 offset = new Vector3 (w / 2, 0, d / 2);
				position = position + offset;

				if (position.x > 0 && position.x < w && position.y > 0 && position.y < h && position.z > 0 && position.z < d) 
				{
					int index = (int)position.x + w * ((int)position.y + h * (int)position.z);
					vals[index] = new Cell(1, (int)position.x, (int)position.y, (int)position.z);
				}
			

			}
		}

        public void initRandom()
        {
            System.Random r = new System.Random();
            vals = new Cell[w * h * d];
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    for (int k = 0; k < d; k++)
                    {
                        int index = i + w * (j + h * k);
						float v = (float)Math.Round(r.NextDouble());
                        vals[index] = new Cell(v, i, j, k);
                    }
                }
            }
        }

        //Utility functions


        //constrain a value to bounds

        private float constrain(float v, float min, float max)
        {
            if (v < min)
            {
                return min;
            }
            else if (v > max)
            {
                return max;
            }
            else return v;
        }

        //map a point to voxel space
        private int[] map(float x, float y, float z)
        {
            int[] mapped = new int[3];
            //leaving divide by 1 for reintroduction of scale property 
            //need to do this mapping properly
            mapped[0] = (int)((x - min[0]) / (1));
            mapped[1] = (int)((y - min[1]) / (1));
            mapped[2] = (int)((z - min[2]) / (1));
            return mapped;
        }

        //get and set values
        public void setValue(float x, float y, float z, float val)
        {
            int[] pt = map(x, y, z);
            setGridValue(pt[0], pt[1], pt[2], val);
        }
        /**
         * Constrains cell modification to within grid. See setValue(Vec3D)
         */
        public void setGridValue(int x, int y, int z, float val)
        {
            if (x >= 0 && x < w && y >= 0 && y < h && z >= 0 && z < d)
            {
                int index = (x) + w * ((y) + h * (z));
                set(index, val);
            }
        }
        /**
         * Sets the value of a cell at a specified index
         * @param index index of cell in the voxel array
         * @param val specified value
         */
        private void set(int index, float val)
        {
            vals[index].set(val);
        }

        public Cell getCell(int x, int y, int z) 
        {
		try {
                if (x > w || x < 0 || y < 0 || y > h || z < 0 || z > d)
                {
                    throw new System.IndexOutOfRangeException();
                }
                int index = x + w * (y + h * z);
                Cell val = vals[index];
                return val;
            } catch (Exception e) {
                return new Cell(-1, x, y, z);
            }


        }

        /**
         * Gets the value of the cell at the specified position in the voxel grid
         * @param x
         * @param y
         * @param z
         * @return value of cell
         */
        public float getValue(int x, int y, int z)
        {
            return getCell(x, y, z).get();
        }


    }
}