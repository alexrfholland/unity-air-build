﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class CAEngine : ThreadedJob
    {
        public VoxelGrid grid;
		VoxelData manager;
		List<Vector3> aliveParticles = new List<Vector3> ();
        
		//List<int> birthRules = new List<int> { 6, 7, 8, 20 };
        //List<int> survivalRules = new List<int> { 5, 6, 7 };

		List<int> birthRules = new List<int> { 6, 7, 8, 20 };
		List<int> survivalRules = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 };

        int nx = 0;
        int ny = 0;
        int nz = 0;
        public int generations = 0;
        public bool running = true;

		public GameObject managerUnity;

		public CAEngine(VoxelGrid _grid, VoxelData _manager)
        {
            grid = _grid;
			manager = _manager;
        }

        public VoxelGrid getData()
        {
            return grid;
        }

        protected override void ThreadFunction()
        {
			aliveParticles.Clear ();
            for (int z = 0; z < grid.d; z++)
            {
                for (int y = grid.h; y > 0; y--)
                {
                    for (int x = 0; x < grid.w; x++)
                    {
                        runBehaviourOnCell(grid.getCell(x, y, z));
                    }
                }
            }
        }
        protected override void OnFinished()
        {
            // This is executed by the Unity main thread when the job is finished
            generations++;
			/*
			if (manager.m_System != null) 
			{
				for (int i = 0; i < aliveParticles.Count; i++) {
					manager.m_Particles [i].position = aliveParticles [i];
				}
				manager.nAlivePart = aliveParticles.Count;
				manager.updateVoxels = true;
			}
			*/
			lock (manager.setVoxLox) {

				manager.newPositions =  aliveParticles.ToArray ();
				manager.updateVoxels = true;
			}
            if(running)this.Start();
        }


        void runBehaviourOnCell(Cell c)
        {
            
            int sum = 0; //sum average data vals
            float cellVal = c.get();

            for(int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        nx = c.x+i;
                        ny = c.y+j;
                        nz = c.z+k;

                         Cell neighbour = grid.getCell(nx, ny, nz);

                         if (neighbour.get() > 0) {
                              sum += 1; //add its value to the sum
                          }
                    }
                }
            }

			c.setNeighbour(sum);

            //ca stuff
            if (cellVal == 0 && birthRules.Contains(sum))
            {
                cellVal = 1;
				aliveParticles.Add (new Vector3 (c.x, c.y, c.z));

            }
            else if (cellVal == 1 && !survivalRules.Contains(sum))
            {
                cellVal = 0;
            }
            else {
                cellVal = 0;
                //if(random(1)<0.01)remove.add(vKey);
            }
            c.set(cellVal);
           
        }

    }
}
