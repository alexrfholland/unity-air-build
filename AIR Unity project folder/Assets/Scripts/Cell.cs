﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Cell
    {
        float val;
        public int x, y, z;
		public int NeighbourCount;


        public Cell(float v, int _x, int _y, int _z)
        {
            val = v;
            x = _x;
            y = _y;
            z = _z;
        }


        public void set(float v) { val = v; }
        public float get() { return val; }

		public void setNeighbour(int num) 
		{
			NeighbourCount = num;
		}
    }
}
