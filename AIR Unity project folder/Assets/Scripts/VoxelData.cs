﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

namespace Assets.Scripts
{
    public class VoxelData : MonoBehaviour
    {
		public object setVoxLox = new object ();

		public  ParticleSystem m_System;
		public  ParticleSystem.Particle[] m_Particles;
		public int nAlivePart, nDeadPart;
		public Vector3[] newPositions;
		public bool updateVoxels = true;
		public float m_Drift = 0.01f;
		public static CAEngine CA;
        float deltaTime = 0.0f;

		public int gridX;
		public int gridY;
		public int gridZ;

		public bool useParticles;



		public bool displayGizmos = false;

		public bool colorNeighbour = false;

		public GameObject unityManager;

		public bool usingVobject;

		public TextAsset[] CSVData;
		public static VObject[] voxelObjects;



        // Use this for initialization
        void Start()
        {
			VoxelGrid grid = new VoxelGrid(gridX, gridY, gridZ, new float[] { 0, 0, 0 }, new float[] { gridX, gridY, gridZ });
            
			if (usingVobject == false) 
			{
				grid.initRandom ();
			}

			if (usingVobject == true)
			{
				//
				grid.initGrid();
				voxelObjects = readData(CSVData);
				grid.loadVobject(voxelObjects[0]);
			}

            CA = new CAEngine(grid, this);
            CA.Start();
			unityManager = gameObject;
            //print("initialised");
            
        }
        
            
            // Update is called once per frame
        void Update()
        {
            // 
            if (Input.GetKeyDown(KeyCode.X))
            {
                CA.running = false;
                CA.Abort();
                print("exited CA");
                print("" + CA.generations + " current cell val at 0,0,0: " + CA.grid.getValue(0, 0, 0));
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                CA.running = true;
                CA.Start();
                print("started CA");
                print("" + CA.generations + " current cell val at 0,0,0: " + CA.grid.getValue(0, 0, 0));

            }

            deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
            //print("updated once");
        }

		public void updateList(Vector3[] newpos) {

		}

        void OnGUI()
        {
            int w = Screen.width, h = Screen.height;

            GUIStyle style = new GUIStyle();

            Rect rect = new Rect(0, 0, w, h * 2 / 100);
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 100;
            style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(rect, text, style);
        }

		void OnDrawGizmos() 
		{
			if (displayGizmos && CA != null && CA.running) 
			{
				for (int x = 0; x < gridX; x++) {
					for (int y = 0; y < gridY; y++) {
						for (int z = 0; z < gridZ; z++) {
								
							if (CA.grid.getCell (x, y, z).get () == 1) {
								Vector3 pos = new Vector3 (x, y, z);


								if (colorNeighbour == false) 
								{
									Gizmos.color = new Vector4 ((float)x / gridX, (float)y / gridY, (float)z / gridZ, 1);
									Gizmos.DrawCube (pos, Vector3.one / 2);
								}

								if (colorNeighbour == true) 
								{
									int tempNeighbour = CA.grid.getCell (x, y, z).NeighbourCount;
									Gizmos.color = new Vector4 ((float) tempNeighbour / 28, (float) 1, 1, 1);
									Gizmos.DrawCube (pos, Vector3.one / 2);
								}
							}
						}
					}
				}
			}
		}




		private void LateUpdate()
		{
			if (useParticles) 
			{
				InitializeIfNeeded ();
				/*
			if (updateVoxels) {
				int noldPart = m_System.GetParticles(m_Particles)
					for (int i = 0; i < nAlivePart; i++) {
						m_Particles[i].position = newPositions[i];
					}
				m_System.SetParticles(m_Particles, nAlivePart);
				updateVoxels = false;
			}
			// GetParticles is allocation free because we reuse the m_Particles buffer between updates
			//int numParticlesAlive = m_System.GetParticles(m_Particles);

			 Change only the particles that are alive
			for (int i = 0; i < numParticlesAlive; i++)
			{
				m_Particles[i].velocity += Vector3.up * m_Drift;
			}
			*/
				lock (setVoxLox) {

					if (updateVoxels) {
						gameObject.GetComponent<PointCloud> ().SetPoints (newPositions);
						updateVoxels = false;
					}
				}
			}

			// Apply the particle changes to the particle system
			//m_System.SetParticles(m_Particles, numParticlesAlive);

		}

		void InitializeIfNeeded()
		{
			if (m_System == null)
				m_System = GetComponent<ParticleSystem>();
			m_System.maxParticles = gridX * gridY * gridZ;

			if (m_Particles == null || m_Particles.Length < m_System.maxParticles)
				m_Particles = new ParticleSystem.Particle[m_System.maxParticles]; 
			

			for (int ii = 0; ii < m_System.maxParticles; ++ii)
			{
				m_Particles[ii].position = new Vector3(CA.grid.vals[ii].x,CA.grid.vals[ii].y,CA.grid.vals[ii].z);            
			}
		}

		private VObject[] readData(TextAsset[] csv)
		{

			char lineSeperater = '\n'; // It defines line seperate character
			char fieldSeperator = ','; // It defines field seperate chracter

			ArrayList tempVobjectList = new ArrayList();


			for (int i = 0; i < csv.Length; i++)
			{

				// create all the temp variables which will be used to create a Vobject
				int tempName = -1;
				int nPoints = 0;
				bool hasNPoints = false;
				bool hasName = false;
				//            bool isLandscape = false;
				bool isObject = false;

				// Separate each row with the lineSeperater character
				string[] records = csv[i].text.Split(lineSeperater);

				// The first row is the setup information, Key/Value pair with even indicies being strings as a key and the odd the values
				string[] SetupRow = records[0].Split(fieldSeperator);

				if (SetupRow[0].Equals("VObject"))
				{
					isObject = true;
				}

				// Loop through each key in the SetupRow, this will be the even elements of the string array, SetupRow[i]
				// that is i=0,2,4 ect so in the for loop not i++ but i=i+2
				for (int j = 1; j < SetupRow.Length; j = j + 2)
				{
					string key = SetupRow[j];
					// On each of thoes keys if they match the following do somehting with the value, which is i+1, the next value in the CSV startup row
					switch (key)
					{
					case "nVoxels":
						// The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
						// The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
						hasNPoints = int.TryParse(SetupRow[j + 1], out nPoints);
						break;
					case "name":
						// The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
						// The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
						int.TryParse(SetupRow[j + 1],out tempName);
						hasName = true;
						break;
					default:
						break;
					}
				}

				if (isObject && hasNPoints && hasName)
				{
					tempVobjectList.Add(getVobject(records, nPoints, tempName));
				}
			}
			if (tempVobjectList.Count > 0)
			{
				return (VObject[])tempVobjectList.ToArray(typeof(VObject));
			}
			else
			{
				return null;
			}
		}

		private VObject getVobject(string[] records, int nPoints, int VobjName)
		{

			char fieldSeperator = ','; // It defines field seperate chracter

			// create all the temp variables which will be used to create a Vobject
			double[][] attribureArrayOut = new double[nPoints][];
			Vector3[] tempPointsList = new Vector3[nPoints];



			// For the rest of the rows read each row and split it up into the different attributes storing them in an arraylist.
			// Remember that the first row is setup so we much check from i=1 not zero!
			for (int j = 1; j < records.Length; j++)
			{
				string[] fields = records[j].Split(fieldSeperator);


				if (fields.Length > 3)
				{

					float tempX, tempY, tempZ;
					if (float.TryParse(fields[0], out tempX) && float.TryParse(fields[2], out tempY) && float.TryParse(fields[1], out tempZ))
					{
						//tempPointsList[j - 1] = new Vector3(tempX, tempY, tempZ);
						tempPointsList[j - 1] = new Vector3(Mathf.Floor(tempX * globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Floor(tempY * globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Floor(tempZ * globalManager.scalingFactor) / globalManager.scalingFactor);

					}

					double[] newAtt = new double[fields.Length - 3];
					// Loop for each attribute, parse (or convert) them from string to double (floating point values)
					for (int k = 3; k < fields.Length; k++)
					{
						if (!double.TryParse(fields[k], out newAtt[k - 3]))
						{
							// error handle
						}
					}
					attribureArrayOut[j - 1] = newAtt;
				}

			}

			return new VObject(tempPointsList, attribureArrayOut, VobjName);
		}
	}

}
