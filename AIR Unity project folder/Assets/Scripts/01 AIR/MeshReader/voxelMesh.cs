﻿using UnityEngine;
using System.Collections;

public class voxelMesh : MonoBehaviour 
{
	Renderer voxelRenderer;
	public bool active = false;
    public Vector3 voxelLoc;
	public GameObject[] voxelMeshes;

	Mesh origMesh;
	Mesh[] newMesh;

	public Vector4 colourVal;
	public int ofVobject;
	public int[] _popHistory;
	public Vector3 prevPosition;
	public Color myColor;
	bool isIntersectional;


	// IGNORE ME! //
	// Use this for initialization
	void Start () 
	{
		voxelRenderer = gameObject.GetComponent<Renderer> ();
		voxelRenderer.enabled = false;

		origMesh = gameObject.GetComponent<MeshFilter> ().mesh;

		newMesh = new Mesh[voxelMeshes.Length];

		for (int i = 0; i < voxelMeshes.Length; i++) 
		{
			newMesh [i] = voxelMeshes[i].GetComponent<MeshFilter> ().mesh;
		}

	}

    // Update is called once per frame
    void Update()
    {
       
		if (transform.position - prevPosition != new Vector3 (0,0,0) )
		{
			//SETUP CODE: IGNORE
			voxelLoc = transform.position;

			string key = voxelManager.getKey(voxelLoc);
			if (voxelManager.globalGrid.ContainsKey (key)) 
			{
				//  voxel in this position
				Voxel thisVoxel = ((Voxel)voxelManager.globalGrid [key]);

				// Check if the voxel is populated
				if (thisVoxel.isPopulated == true) {
					//voxelRenderer.enabled = true;
					ofVobject = thisVoxel.populatedBy;
					_popHistory = thisVoxel.popHistory;

					if (voxelManager.OnlyIntersectional == false) 
					{
						voxelRenderer.enabled = true;

						//don't colour Voxels by type
						if (voxelManager.showVoxelColour == false) 
						{
							voxelRenderer.material.color = Color.white;	
						} 

						///////////////////////////////////////////////////////////////////////////
						////////////////////// REFVO1 ///////////////////////////////////////////
						////////////////////////////////////////////////////////////////////////

						//colour Voxels by type
						else if (voxelManager.showVoxelColour == true) 
						{
							switch (thisVoxel.popHistory [thisVoxel.popHistory.Length - 1]) 
							{

							case 0:

								voxelRenderer.material.color = Color.yellow;	
								break;

							case 1:

								colourVal = new Vector4 (.7f, .2f, 1, 1);
								myColor = colourVal;
								voxelRenderer.material.color = myColor;	

								break;
							case 2:

							//if voxel is of vobject named 2
								colourVal = new Vector4 (.5f, 1, 1, 1);
								myColor = colourVal;
								voxelRenderer.material.color = myColor;	
								break;

							case 3:

							//if voxel is of vobject named 2
								colourVal = new Vector4 (.5f, 1, 1, 1);
								myColor = colourVal;
								voxelRenderer.material.color = myColor;	
								break;

							default:

								colourVal = new Vector4 (.1f, .2f, 1, 1);
								myColor = colourVal;
								voxelRenderer.material.color = myColor;	

								break;
							}
						}

						//if this Voxel is populated by more than one attribute 

						if (thisVoxel.popHistory.Length > 1) 
						{
							//colourVal = new Vector4 (2, 1, 1, 1);
							myColor = Color.green;
							voxelRenderer.material.color = myColor;	
						}
					}

					///////////////////////////////////////////////////////////////////////////
					////////////////////// END REFVO1 ///////////////////////////////////////////
					////////////////////////////////////////////////////////////////////////

			
					//hide voxels that only are populated by one thing
					if (voxelManager.OnlyIntersectional == true) 
					{
						if (thisVoxel.popHistory.Length < 2) 
						{
							voxelRenderer.enabled = false;
						}
					} 

					///////////////////////////////////////////////////////////////////////////
					////////////////////// REFVO2 ///////////////////////////////////////////
					////////////////////////////////////////////////////////////////////////

					//Check to see if populated by type 4 and type 1
					foreach (int checkA in thisVoxel.popHistory) 
					{
						if (checkA == 4) 
						{
							foreach (int checkB in thisVoxel.popHistory) 
							{
								if (checkB == 1) 
								{
									//colourVal = new Vector4 (2, 1, 1, 1);
									voxelRenderer.enabled = true;
									myColor = Color.green;
									voxelRenderer.material.color = myColor;	

								} 
							}
						}
					}

					///////////////////////////////////////////////////////////////////////////
					////////////////////// END REFVO2 ///////////////////////////////////////////
					////////////////////////////////////////////////////////////////////////
				}
			}

			else
			{
				// There is no voxel in that location.
				voxelRenderer.enabled = false;
			}
		}
    }
}

/* //////////////////////////////////////////////////////////////////////////////////////
//////////////////////     EXAMPLE CODE  ////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////////////////

//if this Voxel is populated by more than one attribute 

					if (thisVoxel.popHistory.Length > 1) 
					{
						//colourVal = new Vector4 (2, 1, 1, 1);
						myColor = Color.green;
						voxelRenderer.material.color = myColor;	
					}

			
//if this Voxel is populated by more than two  things

					if (thisVoxel.popHistory.Length < 2) 
					{
						//colourVal = new Vector4 (2, 1, 1, 1);
						gameObject.SetActive (false);
					}
*/



//SUPERSCEDED - IGNORE

/*if (string.Equals(thisVoxel.populatedBy[0],"Cone")) 
				{
					//gameObject.GetComponent<MeshFilter> ().mesh = newMesh [0];


					colourVal = new Vector4( 2, 1, 1, 1);
					Color myColor = colourVal;
					voxelRenderer.material.color = myColor;

				}
*/