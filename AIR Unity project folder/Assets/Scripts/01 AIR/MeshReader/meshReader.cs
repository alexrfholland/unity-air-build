﻿using UnityEngine;
using System.Collections;

public class meshReader : MonoBehaviour {

	public GameObject voxelMesh;

	public static int voxelReaderX;
    public static int voxelReaderY;
	public static int voxelReaderZ;

	public int setVoxelReaderX;
	public int setVoxelReaderY;
	public int setVoxelReaderZ;

    public float bubble = 4;

	void Awake()
	{
		voxelReaderX = setVoxelReaderX / 2;
		voxelReaderY = setVoxelReaderY;
		voxelReaderZ = setVoxelReaderZ / 2;
	}

		
    // Use this for initialization
    void Start()
    {

        for (float i = -voxelReaderX; i < voxelReaderX; i++)
        {
            for (float j = 0; j < voxelReaderY; j++)
            {
                for (float k = -voxelReaderZ; k < voxelReaderZ; k++)
                {
					Vector3 pos = new Vector3(i / globalManager.combinedScalingFactor, j / globalManager.combinedScalingFactor, k / globalManager.combinedScalingFactor);
                        Quaternion rot = Quaternion.identity;
                        GameObject newObj = Instantiate(voxelMesh, pos, rot) as GameObject;
                        newObj.transform.parent = gameObject.transform;
                }
            }
        }

    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
