﻿using UnityEngine;
using System.Collections;

public class voxelReaderClamp : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{
		transform.position = new Vector3(Mathf.Floor((player.transform.position.x) * globalManager.combinedScalingFactor) / globalManager.combinedScalingFactor, 0, Mathf.Floor((player.transform.position.z) * globalManager.combinedScalingFactor) / globalManager.combinedScalingFactor);
	}
}


