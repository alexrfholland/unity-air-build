﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class tileManager : MonoBehaviour {


	public GameObject tile;
	public bool trace;
	public GameObject obj;

	public int att = 1;

	int x;
	int y;

	public int rows ;
	public int cols ;
	public int tileCount;
    

	public TextAsset csvFile; // Reference of CSV file
	private char lineSeperater = '\n'; // It defines line seperate character
	private char fieldSeperator = ','; // It defines field seperate chracter

	public ArrayList attributes = new ArrayList ();
    

	public int GridWidth = -1;
	public int GridDepth = -1;
	public int GridHeight = -1;

	public bool[] hasVisited;
	public bool[] hasSpawned;
    public static bool[,] isDead;
    public static bool[,] isVisited;

	public int [] timesVisited;

	public static int gridXDim;
	public static int gridZDim;

	public float slaveScaleFactor;



	// Use this for initialization
	void Start () 
	{	 

		// running the function readData to textAsset csvFile, 
		//att is an empty array list (dynamic array) that is populated with ReadData
		//we don't know how many items are in the array (the list in CSV)
		// The GridWidth gets populated with this function
		attributes = readData(csvFile);

		gridXDim = GridWidth;
		gridZDim = GridDepth;

		slaveScaleFactor = globalManager.scalingFactor;


		//data of visited cells in grid
		//static arrays of attributes.Count size
		hasVisited = new bool[attributes.Count];
		hasSpawned = new bool[attributes.Count];
        isDead = new bool[GridWidth,GridDepth];
        isVisited = new bool[GridWidth, GridDepth];

		timesVisited = new int[attributes.Count];

		for (int i = 0; i < attributes.Count; i++)
		{
			hasSpawned[i] = false;
		}

        for (int julian = 0; julian < GridWidth; julian++)
        {
            for (int carmel = 0; carmel < GridDepth; carmel++)
            {
                isDead[julian, carmel] = false;
                isVisited[julian, carmel] = false;
            }
        }




		//double array is an array of floats
		//debug stuff
		/*for (int i = 0; i < Att1.Count; i++) {
			string newline = "";
			for (int j = 0; j < ((double[])Att1[i]).Length; j++) {
				newline = newline + string.Format("{0} , {1} , {2}", ((double[])Att1[i])[j],i,j);
			}
			Debug.Log (newline);
		}
*/

		//tile generation
		for (x = -rows; x < rows; x++) 
		{
			for (y = -cols; y < cols; y++) 
			{

				//spawning tiles
				Vector3 pos = new Vector3 (x / slaveScaleFactor, 0, y / slaveScaleFactor);
				Quaternion rot = Quaternion.identity;
				GameObject newObj = Instantiate (tile, pos, rot) as GameObject;
				newObj.transform.parent = gameObject.transform;

				tileCount++;

			}
		}
	}
		
	void Update ()
	{

	}

	// Read data from CSV file
	/// <summary>
	/// Reads the data.
	/// </summary>
	/// <returns>The data.</returns>
	/// <param name="csv">Csv.</param>

	//
	private ArrayList readData(TextAsset csv)
	{
		// create an array list to add all the attributes to as they are read from the CSV file
		ArrayList outArrayList = new ArrayList();

		// Separate each row with the lineSeperater character
		string[] records = csv.text.Split(lineSeperater);

		// The first row is the setup information, Key/Value pair with even indicies being strings as a key and the odd the values
		string[] SetupRow = records[0].Split(fieldSeperator);

		// Loop through each key in the SetupRow, this will be the even elements of the string array, SetupRow[i]
		// that is i=0,2,4 ect so in the for loop not i++ but i=i+2
		for (int i = 0; i < SetupRow.Length; i = i + 2)
		{
			string key = SetupRow[i];
			// On each of thoes keys if they match the following do somehting with the value, which is i+1, the next value in the CSV startup row
			switch (key)
			{
			case "GridWidth":
				// The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
				// The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
				if (!int.TryParse(SetupRow[i + 1], out GridWidth))
				{
					// Error message here, could not read the GridWidth!!! 
				}
				break;
			case "GridDepth":
				// The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
				// The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
				if (!int.TryParse(SetupRow[i + 1], out GridDepth))
				{
					// Error message here, could not read the GridDepth!!! 
				}
				break;
			default:
				break;
			}
		}

		// For the rest of the rows read each row and split it up into the different attributes storing them in an arraylist.
		// Remember that the first row is setup so we much check from i=1 not zero!
		for (int i = 1; i < records.Length; i++)
		{
			string[] fields = records[i].Split(fieldSeperator);

			double[] newAtt = new double[fields.Length];
			// Loop for each attribute, parse (or convert) them from string to double (floating point values)
			for (int j = 0; j < fields.Length; j++)
			{
				if (!double.TryParse(fields[j], out newAtt[j]))
				{
					// error handle
				}
			}
			outArrayList.Add(newAtt);
		}

		return outArrayList;
	}

	//function
	public int convertXYZtoI(float myX, float myZ)
	{
		int indicie = (int) ((myX * GridWidth + myZ) * globalManager.scalingFactor);
		return indicie;
	}
		

	public int convertXYZtoI(Vector3 vecPos) 
	{
		//return convertXYZtoI (((float) (Mathf.RoundToInt (vecPos.x)))/globalManager.scalingFactor, ((float) (Mathf.RoundToInt (vecPos.z)))/globalManager.scalingFactor);
		return convertXYZtoI (Mathf.Floor(vecPos.x * globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Floor(vecPos.z * globalManager.scalingFactor) / globalManager.scalingFactor);

	}

    public int ItoX (int i)
    {
        return i % GridWidth;
    }
    public int ItoZ(int i)
    {
        return i / GridDepth;
    }


}

