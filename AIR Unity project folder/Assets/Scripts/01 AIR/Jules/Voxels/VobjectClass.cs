﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Voxel object class 
/// </summary>
public struct VObject 
{
	// variables
	public Voxel[] geometry;
	public int objectName;


	// Properties

	// Constructor
	public VObject(Vector3[] _GeometryPoints, double[][] _Attributes, int _objectName)
	{
		objectName = _objectName;

		geometry = new Voxel[_GeometryPoints.Length];

		for (int i = 0; i < _GeometryPoints.Length; i++)
		{
			geometry[i] = new Voxel(_GeometryPoints[i], _objectName);
		}
	}

	// Methods


}