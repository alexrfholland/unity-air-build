﻿using UnityEngine;
using System.Collections;

/// <summary>
	/// This is a Voxel class which holds all the information to do shit with voxels
	/// </summary>
	public struct Voxel 
	{
		// Variables
		public double[] attributes;
		public bool isPopulated;
		public bool isDead;
		public int populatedBy; //this used to be a string
		public Vector3 localOrigin;

		public int[] popHistory;

		// Properties


		/////////////////////////////// 
		// Constructer
		//////////////////////////////


		/*
		/// <summary>
		/// This is the base constructer. This will create a Voxel with default values of bools, other information are null.
		/// </summary>
		public void setupVoxel()
		{
			attributes = new double[]{0};
			isPopulated = false;
			isDead = false;
			populatedBy = new string[]{""};
			localOrigin = Vector3.zero;
		}

		/// <summary>
		/// This will create a Voxel with a local origin. Then it will call the base constructer to populate the default information
		/// </summary>
		/// <param name="_localOrigin"></param> This is the local origin which will be the XYZ location relative to grasshopper / rhino.
		/// The XYZ coordinates have been converted when the CSV file was read. (where Z is up in rhino and Y is up in unity)
		public Voxel(Vector3 _localOrigin)  // This is the default Voxel() constructer
		{
			setupVoxel();
			localOrigin = _localOrigin;
		}
		*/

		public Voxel(Vector3 _localOrigin, int VobjectName)
		{

			attributes = new double[]{0};
			isPopulated = false;
			isDead = false;
			localOrigin = _localOrigin;
			populatedBy = VobjectName;

			popHistory = new int[]{ VobjectName };
		}

		/// <summary>
		/// This will populate the Voxel with a local origin and a list of attributes.
		/// </summary>
		/// <param name="_localOrigin"></param>This is the local origin which will be the XYZ location relative to grasshopper / rhino.
		/// The XYZ coordinates have been converted when the CSV file was read. (where Z is up in rhino and Y is up in unity)
		/// <param name="_Attributes"></param>This is the attributes read from the CSV file.

		//FOR FUTURE
		public void updateAttributes(double[] _Attributes)
		{
			attributes = new double[_Attributes.Length];

			for (int i = 0; i < _Attributes.Length; i++)
			{
				attributes[i] = _Attributes[i];
			}
		}

		public void addHistory (int latestVobjectName)
		{
			int[] tempHistory = new int[popHistory.Length + 1];

			for (int i = 0; i < popHistory.Length; i++) 
			{
				tempHistory [i] = popHistory [i];
			}

			tempHistory [tempHistory.Length - 1] = latestVobjectName;

			popHistory = tempHistory;

		}

		// Methods

		// Static stuff

	}


