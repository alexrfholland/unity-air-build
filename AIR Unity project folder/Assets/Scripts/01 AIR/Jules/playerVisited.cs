﻿using UnityEngine;
using System.Collections;

public class playerVisited : MonoBehaviour {

	public GameObject manager;

	public float gridWidth;
	public float gridDepth;

	public float myX;
	public float myZ;

	public float slaveAtt1;
	public float slaveAtt2;
	public float slaveAtt3;

	public int PlayerNumber;

//	GameObject obj;

	bool hasSpawned = false;
	public float timeLimit = 3;

	public int NewMethodPlayerNo;

//	bool setup = false;




	// Use this for initialization
	void Start () 
	{
//		obj = manager.GetComponent<tileManager> ().obj;
		InvokeRepeating ("timesVisited", 0, .1f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		gridWidth = (float) manager.GetComponent<tileManager> ().GridWidth;
		gridDepth = (float) manager.GetComponent<tileManager> ().GridDepth;


		myX = Mathf.Floor (transform.position.x * globalManager.scalingFactor) / globalManager.scalingFactor;
		myZ = Mathf.Floor (transform.position.z * globalManager.scalingFactor) / globalManager.scalingFactor;


		if ((myX >= 0 && myX <= gridWidth) && (myZ >= 0 && myZ <= gridDepth)) 
		{
			PlayerNumber = (int)manager.GetComponent<tileManager> ().convertXYZtoI (transform.position);

			slaveAtt1 = getAttribute (0);
			slaveAtt2 = getAttribute (1);
			slaveAtt3 = getAttribute (2);

		} 

		else 
		
		{
			
			slaveAtt1 = 0;
			slaveAtt1 = 0;
			slaveAtt3 = 0;
		
		}
	
		

			
		//visited ();


       /* DELETE
        * 
        * if (voxelManager.globalGrid.ContainsKey(voxelManager.getKey(new Vector3(myX, 0.5f, myZ))))
        {
            float yVal = 0.5f;
            while (voxelManager.globalGrid.ContainsKey(voxelManager.getKey(new Vector3(myX, yVal, myZ))))
            {
                ((Voxel)voxelManager.globalGrid[voxelManager.getKey(new Vector3(myX, yVal, myZ))]).isDead = true;
                yVal += 0.5f;
               // yield return new WaitForSeconds(0.3f);
            }
        }

		*/
        
		/*
		if ((myX >= 0 && myX <= (gridWidth / globalManager.scalingFactor) && (myZ >= 0 && myZ <= (gridDepth / globalManager.scalingFactor)))) 
		{
			hasSpawned = manager.GetComponent<tileManager> ().hasSpawned [PlayerNumber];
		}

		if (manager.GetComponent<tileManager> ().timesVisited [PlayerNumber] > timeLimit && hasSpawned == false) 
		{
			//spawnBlock ();
		}

		if (manager.GetComponent<tileManager> ().hasVisited [PlayerNumber] == true && hasSpawned == false) 
		{
			//Invoke ("spawnBlock", .2f);
		}
		*/
	}


	void visited ()
	{
//		manager.GetComponent<tileManager> ().hasVisited[PlayerNumber] = true;
	}


	void timesVisited ()
	{
//		manager.GetComponent<tileManager> ().timesVisited [PlayerNumber]++;
	}

	//void spawnBlock ()
	//{
	//	Vector3 pos = gameObject.transform.position;
	//	Quaternion rot = Quaternion.identity;
	//	GameObject spawn = Instantiate (obj, pos, rot) as GameObject;
	//	manager.GetComponent<tileManager> ().hasSpawned [PlayerNumber] = true;
	//}


	float getAttribute(int attribute)
	{

		//error check
		//gets list all of arrays for all of tiles
		ArrayList tempList = (ArrayList)(manager.GetComponent<tileManager> ().attributes);

		//checks if this list is empty
		if (tempList != null && tempList.Count > 0) {
			//gets a list of attributes for this tile
			if (tempList.Count > PlayerNumber && PlayerNumber >= 0) {

				double[] tempdouble = (double[])tempList [PlayerNumber];

				//checks if this list is empty
				if (tempdouble != null && tempdouble.Length > 0) {

					//returns this attribute
					return (float)tempdouble [attribute];
				}
			}
		}
		return 0f;
	}
}
