﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class readAsset : MonoBehaviour 
{
	public TextAsset csvFile; // Reference of CSV file

	private char lineSeperater = '\n'; // It defines line seperate character
	private char fieldSeperator = ','; // It defines field seperate chracter

	void Start () 
	{
		readData ();
	}

	// Read data from CSV file
	private ArrayList readData()
	{

		ArrayList outArrayList = new ArrayList();
		string[] records = csvFile.text.Split(lineSeperater);
		foreach (string record in records)
		{
			string[] fields = record.Split(fieldSeperator);

			double[] newAtt = new double[fields.Length];
			for (int i = 0; i < fields.Length; i++)
			{
				if (!double.TryParse(fields[i], out newAtt[i]))
				{
					// error handle
				}
			}
			outArrayList.Add(newAtt);
		}
		return outArrayList;
	}
}