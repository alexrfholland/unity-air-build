﻿using UnityEngine;
using System.Collections;

public class tileColour : MonoBehaviour
{

    public GameObject obj;
    GameObject Geo2;

    public GameObject manager;
    public GameObject player;

    public float accumColor = 0;

    Renderer myRenderer;
    public GameObject thisObject;

    public int attributeA;

    public int visitedTileNo;
    public bool slaveVisited = false;

    public int slaveTimesVisited;
    bool hasSpawned = false;

    public Vector4 colourVal;

    public int myX;
    public int myZ;

	public int[] deadZone;
	int shapeInt;

	public int _gridXDim;
	public int _gridZDim;

	public float Treeness;
	public float Pathness;
	public float Noiseness;
	public float Interestingness;




    bool tileVis;

    // IGNORE ME //
	// Use this for initialization
    void Start()
    {
		myRenderer = gameObject.GetComponent<Renderer>();

		_gridXDim = tileManager.gridXDim;
		_gridZDim = tileManager.gridZDim;

		deadZone = voxelManager.deadZones;


    }

	// IGNORE ME
    // Update is called once per frame
    void Update()
    {

        myX = Mathf.RoundToInt(transform.position.x);
        myZ = Mathf.RoundToInt(transform.position.z);


        //accumColor = attributA
        if (manager.GetComponent<tileManager>().att == 1)
        {
            accumColor = thisObject.GetComponent<xyzToI>().slaveAtt1;
        }

        else if (manager.GetComponent<tileManager>().att == 2)
        {
            accumColor = thisObject.GetComponent<xyzToI>().slaveAtt2;
        }

        else if (manager.GetComponent<tileManager>().att == 3)
        {
            accumColor = thisObject.GetComponent<xyzToI>().slaveAtt3;
        }

		else if (manager.GetComponent<tileManager>().att == 4)
		{
			accumColor = thisObject.GetComponent<xyzToI>().slaveAtt4;
		}

		///////////////////////////////////////////////////////////////////////////
		////////////////////// ATTRIBUTES ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////

		Treeness = thisObject.GetComponent<xyzToI>().slaveAtt1;
		Noiseness = thisObject.GetComponent<xyzToI>().slaveAtt2;
		Interestingness = thisObject.GetComponent<xyzToI>().slaveAtt3;
		Pathness = thisObject.GetComponent<xyzToI>().slaveAtt4;


		///////////////////////////////////////////////////////////////////////////
		////////////////////// END ATTRIBUTES ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


        myRenderer.enabled = false;

        visitedTileNo = thisObject.GetComponent<xyzToI>().TileNo;

        slaveVisited = manager.GetComponent<tileManager>().hasVisited[visitedTileNo];
        slaveTimesVisited = manager.GetComponent<tileManager>().timesVisited[visitedTileNo];
        hasSpawned = manager.GetComponent<tileManager>().hasSpawned[visitedTileNo];


        if (manager.GetComponent<tileManager>().trace == true)
        {
            // visited colour
            if (slaveVisited == true && thisObject.GetComponent<xyzToI>().tileWithinRange)
            {
                myRenderer.enabled = true;
                colourVal = new Vector4((float)slaveTimesVisited / 100, (float)slaveTimesVisited / 100, (float)slaveTimesVisited / 100, 1);
                Color myColor = colourVal;
                //Color myColor = new Vector4 (slaveTimesVisited/10, slaveTimesVisited/10, slaveTimesVisited/10, 1);
                myRenderer.material.color = myColor;
            }
        }

        if (manager.GetComponent<tileManager>().trace == false)
        {
            if (accumColor > 0)
            {
                myRenderer.enabled = true;
                Color myColor = new Vector4(accumColor, accumColor, accumColor, 1);
                myRenderer.material.color = myColor;
            }
        }


		/// ////////////////////////////////////////////////////////
		/////////////////////////// REFTC1 ///////////////////////////
		/// ///////////////////////////////////////////////////////
		 
		if (Pathness > 0.8f && Pathness <= 1 && hasSpawned == false) 
		{
			shapeInt = 0;
			shapeToHash ();
		}
		 
		if (Interestingness > 0.8f && accumColor <= 1 && hasSpawned == false) 
		{
			shapeInt = 1;
			shapeToHash ();
		}
			
		/*

		//there is a 50% chance of spawning this shape
		if (Pathness > 0.5f && Pathness <= 0.6 && hasSpawned == false) 
		{
			int SpawnChance = Random.Range (0, 2);
			if (SpawnChance > 0) {
				shapeInt = 0;
				shapeToHash ();
			} 
			else 
			{
				manager.GetComponent<tileManager> ().hasSpawned [visitedTileNo] = true;
			}
		}

		if (accumColor > 0.3f && accumColor <= 0.4f && hasSpawned == false) 
		{
			shapeInt = 2;
			shapeToHash ();
		}

		if (accumColor > 0.4f && accumColor <= 0.5f && hasSpawned == false) 
		{
			shapeInt = 3;
			shapeToHash ();
		}

		if (accumColor > 0.5f && accumColor <= 0.6f && hasSpawned == false) 
		{
			shapeInt = 4;
			shapeToHash ();
		}

		if (accumColor > 0.7f && accumColor <= 0.8f && hasSpawned == false) 
		{
			shapeInt = 5;
			shapeToHash ();
		}


		/*
		if (accumColor > 0.7f && accumColor <= 1 && hasSpawned == false) 
		{
			shapeInt = 1;
			shapeToHash ();
		}

	
		if (accumColor > .8f && accumColor <= 1 && hasSpawned == false) 
		{
			shapeInt = 3;
			shapeToHash ();
		}

		*/

		///////////////////////////////////////////////////////////////////////////////////
		/// ////////////////////// END REF1TC ///////////////////////////////////
		/// ////////////////////////////////////////////////////////////////////////////

		if (myX >= 0 && myX < _gridXDim && myZ >= 0 && myZ < _gridZDim) 
		{
			if (tileManager.isDead [myX, myZ] == true && voxelManager.showDeadZone == true) 
			{
				myRenderer.enabled = true;
				myRenderer.material.color = Color.red;
			}
		}	

 	}



    

	 
	void shapeToHash ()
	{
		if (tileManager.isDead [myX, myZ] == false) 
		{
			// make deadzone
			for (int x = myX - deadZone [shapeInt]; x < myX + deadZone [shapeInt]; x++) 
			{
				for (int z = myZ - deadZone [shapeInt]; z < myZ + deadZone [shapeInt]; z++) 
				{
					
					if (x >= 0 && x < _gridXDim && z >= 0 && z < _gridZDim) 
					{
						tileManager.isDead [x, z] = true;

					}	
				}
			}

			// update hashtable
			Vector3 pos = gameObject.transform.position;
			if (voxelManager.voxelObjects != null) {
				voxelManager.spawn (voxelManager.voxelObjects [shapeInt], pos);
				manager.GetComponent<tileManager> ().hasSpawned [visitedTileNo] = true;
			}
		}
	}


}