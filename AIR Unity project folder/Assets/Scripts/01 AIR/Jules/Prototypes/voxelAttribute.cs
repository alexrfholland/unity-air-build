﻿using UnityEngine;
using System.Collections;

public class voxelAttribute : MonoBehaviour {

	public int myX;
	public int myZ;
	public int myY;

	public int gridWidth;
	public int TileNo;

	public GameObject manager;

	public int slaveTileCount;
	public float slaveAtt1;
	public float slaveAtt2;
	public float slaveAtt3;

	public bool slaveVisited = false;
	public int slaveTimesVisited = 0;

	public Vector3 XYZPoint;
	public Vector3 voxelClamp;

	Renderer myRenderer;


	void Start () 
	{
	}

	// Update is called once per frame
	void Update ()
	{
		myX = Mathf.RoundToInt (transform.position.x);
		myZ = Mathf.RoundToInt (transform.position.z);
		myY = Mathf.RoundToInt (transform.position.y);

		if ((myX >= 0 && myX <= 100) && (myZ >= 0 && myZ <= 100)) {

			TileNo = myZ + myX * gridWidth + myY * (gridWidth * gridWidth);

			//TileNo = (myX - 1) * gridWidth + myZ;

			slaveTileCount = TileNo + 1;

			slaveAtt1 = getAttribute (0);
			slaveAtt2 = getAttribute (2);
			slaveAtt3 = getAttribute (1);

		} else 

		{
			slaveAtt1 = 0;
			slaveAtt1 = 0;
			slaveAtt3 = 0;
		}

		//just to display
//		slaveVisited = manager.GetComponent<tileManager> ().hasVisited[TileNo];
//		slaveTimesVisited = manager.GetComponent<tileManager> ().timesVisited [TileNo];

		XYZPoint = new Vector3 (slaveAtt1, slaveAtt2, slaveAtt3);
		voxelClamp = new Vector3 (myX, myY, myZ);


		if (voxelClamp == XYZPoint) 
		{
			gameObject.SetActive(true);
		} else 
		{
			gameObject.SetActive (false);
		}

	}

	float getAttribute(int attribute) 
	{

		//error check
		//gets list all of arrays for all of tiles
		ArrayList tempList = (ArrayList)(manager.GetComponent<gridManager> ().attributes);

		//checks if this list is empty
		if (tempList != null && tempList.Count > 0) {
			//gets a list of attributes for this tile
			if (tempList.Count > slaveTileCount && slaveTileCount >= 0) {

				double[] tempdouble = (double[])tempList [slaveTileCount];

				//checks if this list is empty
				if (tempdouble != null && tempdouble.Length > 0) {

					//returns this attribute
					return (float)tempdouble [attribute];
				}
			}
		}
		return 0f;
	}



}
