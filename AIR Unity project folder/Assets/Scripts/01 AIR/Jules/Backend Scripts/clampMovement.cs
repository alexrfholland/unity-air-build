﻿using UnityEngine;
using System.Collections;

public class clampMovement : MonoBehaviour {

	public GameObject player;
	public int offset = 25;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//transform.position = player.transform.position;
		//transform.position = new Vector3 (Mathf.RoundToInt(player.transform.position.x + offset), 0, Mathf.RoundToInt(player.transform.position.z + offset));

		transform.position = new Vector3(Mathf.Floor((player.transform.position.x) * globalManager.scalingFactor) / globalManager.scalingFactor, 0, Mathf.Floor((player.transform.position.z) * globalManager.scalingFactor) / globalManager.scalingFactor);

	}
}
