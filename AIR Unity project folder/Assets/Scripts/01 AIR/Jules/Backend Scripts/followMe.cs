﻿using UnityEngine;
using System.Collections;

public class followMe : MonoBehaviour {

	public GameObject character;
	public bool isScaled = false;

	// Use this for initialization
	void Start () 
	{
	
	}

	// Update is called once per frame
	void Update () 
	{

		if (isScaled == true) 
		{
			transform.position = (character.transform.position * 100);
		
		} 

		else 
		{
			transform.position = character.transform.position;
		}
	
	}
}
