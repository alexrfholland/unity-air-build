﻿using UnityEngine;
using System.Collections;

public class GridVisualiser : MonoBehaviour {

	public GameObject manager;

	public int cols;
	public int rows;

	public Vector3 XandZ;
	public int indice;

	public int TOTALROWSGridWidth;
	public int TOTALCOLSGridDepth;


	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
	
		XandZ = new Vector3 (cols, 0, rows);
		//indice = manager.GetComponent<tileManager> ().convertXYZtoI (XandZ);
		transform.position = XandZ;

		TOTALROWSGridWidth = manager.GetComponent<tileManager> ().GridWidth;
		TOTALCOLSGridDepth = manager.GetComponent<tileManager> ().GridDepth;


	}
}
