﻿using UnityEngine;
using System.Collections;

public class voxelManager : MonoBehaviour
{
    // Variables
    public TextAsset[] CSVData;

	public static int[] deadZones;
	public int[] setDeadZones;

    public static Hashtable globalGrid;
    public static VObject[] voxelObjects;

	public static int geoSelect;
	public static int geoNumber;

	public int setGeo;

	public bool setOnlyIntersectional = false;
	public static bool OnlyIntersectional;

	public static bool showVoxelColour;
	public bool setShowVoxelColour;

	public static bool showDeadZone;
	public bool setShowDeadZone;



	void Awake()
	{
		//creates an array this big
		globalGrid = new Hashtable();

		// Loads the CSV data, this is the landscape info and the voxel objects.
		voxelObjects = readData(CSVData);

		geoSelect = setGeo;

		geoNumber = CSVData.Length;

		deadZones = setDeadZones;


	}

	void Update ()
	{
		showDeadZone = setShowDeadZone;
		showVoxelColour = setShowVoxelColour;
		OnlyIntersectional = setOnlyIntersectional;

	}

    public static string getKey(Vector3 position)
    {
		return string.Format("{0:0.000},{1:0.000},{2:0.000}", Mathf.Round(position.x * globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Round(position.z * globalManager.scalingFactor)/globalManager.scalingFactor, Mathf.Round(position.y * globalManager.scalingFactor)/globalManager.scalingFactor);
    }

    public static Voxel getVoxel(Vector3 transformation)
    {
		return (Voxel)globalGrid[string.Format("{0:0.000},{1:0.000},{2:0.000}", Mathf.Round(transformation.x*globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Round(transformation.z*globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Round(transformation.y*globalManager.scalingFactor) / globalManager.scalingFactor)];
    }


    public static void spawn(VObject spawnObject, Vector3 position)
    {
        for (int i = 0; i < spawnObject.geometry.Length; i++)
        {
            Vector3 pos = position + spawnObject.geometry[i].localOrigin;

            string key = getKey(pos);
            if (globalGrid.ContainsKey(key))
            {
                // a voxel is already here. What would you like to do?
				// Key is New voxel is already in the Hashtable
                // alive voxel already in this position
               // Voxel currentVoxel = (Voxel)globalGrid[key];

				Voxel tempVoxel = (Voxel)globalGrid[key];
				tempVoxel.addHistory (spawnObject.objectName);
				globalGrid.Remove (key);
				globalGrid.Add (key,tempVoxel);

            }
            else
            {
				Voxel newVoxel = new Voxel(pos,spawnObject.objectName);
                newVoxel.isPopulated = true;
                globalGrid.Add(key, newVoxel);
            }
        }
    }



    /// <summary>
    /// Reads CSV data which is an array of CSV textAssets including the Voxel Objects and the landscape data.
    /// The landscape data updates the hashtable while the Voxel objects are returned.
    /// </summary>
    /// <param name="csv"></param>
    /// <returns></returns>
    private VObject[] readData(TextAsset[] csv)
    {

        char lineSeperater = '\n'; // It defines line seperate character
        char fieldSeperator = ','; // It defines field seperate chracter

        ArrayList tempVobjectList = new ArrayList();


        for (int i = 0; i < csv.Length; i++)
        {

            // create all the temp variables which will be used to create a Vobject
            int tempName = -1;
            int nPoints = 0;
            bool hasNPoints = false;
            bool hasName = false;
//            bool isLandscape = false;
            bool isObject = false;

            // Separate each row with the lineSeperater character
            string[] records = csv[i].text.Split(lineSeperater);

            // The first row is the setup information, Key/Value pair with even indicies being strings as a key and the odd the values
            string[] SetupRow = records[0].Split(fieldSeperator);

            if (SetupRow[0].Equals("VObject"))
            {
                isObject = true;
            }

            // Loop through each key in the SetupRow, this will be the even elements of the string array, SetupRow[i]
            // that is i=0,2,4 ect so in the for loop not i++ but i=i+2
            for (int j = 1; j < SetupRow.Length; j = j + 2)
            {
                string key = SetupRow[j];
                // On each of thoes keys if they match the following do somehting with the value, which is i+1, the next value in the CSV startup row
                switch (key)
                {
                    case "nVoxels":
                        // The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
                        // The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
                        hasNPoints = int.TryParse(SetupRow[j + 1], out nPoints);
                        break;
                    case "name":
                        // The number is stored as a string not an int, so convert it to an int and give error reporting if it doesnt work
                        // The tryParse will convert the string to an int, if it fails it returns false, so having the ! will exicute when failed
					 int.TryParse(SetupRow[j + 1],out tempName);
                        hasName = true;
                        break;
                    default:
                        break;
                }
            }

            if (isObject && hasNPoints && hasName)
            {
                tempVobjectList.Add(getVobject(records, nPoints, tempName));
            }
        }
        if (tempVobjectList.Count > 0)
        {
            return (VObject[])tempVobjectList.ToArray(typeof(VObject));
        }
        else
        {
            return null;
        }
    }



    /// <summary>
    /// Loads and returns Voxel Objects from a CSV which has been split into each row and loaded number of points in the object and the object name.
    /// </summary>
    /// <param name="records"></param>
    /// <param name="nPoints"></param>
    /// <param name="VobjName"></param>
    /// <returns></returns>
    private VObject getVobject(string[] records, int nPoints, int VobjName)
    {

        char fieldSeperator = ','; // It defines field seperate chracter

        // create all the temp variables which will be used to create a Vobject
        double[][] attribureArrayOut = new double[nPoints][];
        Vector3[] tempPointsList = new Vector3[nPoints];



        // For the rest of the rows read each row and split it up into the different attributes storing them in an arraylist.
        // Remember that the first row is setup so we much check from i=1 not zero!
        for (int j = 1; j < records.Length; j++)
        {
            string[] fields = records[j].Split(fieldSeperator);


            if (fields.Length > 3)
            {

                float tempX, tempY, tempZ;
                if (float.TryParse(fields[0], out tempX) && float.TryParse(fields[2], out tempY) && float.TryParse(fields[1], out tempZ))
                {
                    //tempPointsList[j - 1] = new Vector3(tempX, tempY, tempZ);
					tempPointsList[j - 1] = new Vector3(Mathf.Floor(tempX * globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Floor(tempY * globalManager.scalingFactor) / globalManager.scalingFactor, Mathf.Floor(tempZ * globalManager.scalingFactor) / globalManager.scalingFactor);

                }

                double[] newAtt = new double[fields.Length - 3];
                // Loop for each attribute, parse (or convert) them from string to double (floating point values)
                for (int k = 3; k < fields.Length; k++)
                {
                    if (!double.TryParse(fields[k], out newAtt[k - 3]))
                    {
                        // error handle
                    }
                }
                attribureArrayOut[j - 1] = newAtt;
            }

        }

        return new VObject(tempPointsList, attribureArrayOut, VobjName);
    }
}



/*
    /// <summary>
/// Loads the landscape layer from a CSV which is split into each row
/// </summary>
/// <param name="records"></param>
private void loadLandscape(string[] records)
{
	char fieldSeperator = ','; // It defines field seperate chracter
	Vector3 tempPoint = Vector3.zero;
	double[] tempAttribute;
	bool hasloadedPoint;
	// For the rest of the rows read each row and split it up into the different attributes storing them in an arraylist.
	// Remember that the first row is setup so we much check from i=1 not zero!
	for (int j = 1; j < records.Length; j++)
	{
		string[] fields = records[j].Split(fieldSeperator);
		hasloadedPoint = false;

		if (fields.Length > 4)
		{

			float tempX, tempY, tempZ;
			if (float.TryParse(fields[0], out tempX) && float.TryParse(fields[2], out tempY) && float.TryParse(fields[1], out tempZ))
			{
				tempPoint = new Vector3(tempX, tempY, tempZ);
				hasloadedPoint = true;
			}

			tempAttribute = new double[fields.Length - 3];
			// Loop for each attribute, parse (or convert) them from string to double (floating point values)
			for (int k = 3; k < fields.Length; k++)
			{
				if (!double.TryParse(fields[k], out tempAttribute[k - 3]))
				{
					// error handle
				}
			}
			if (hasloadedPoint)
			{
				string key = getKey(tempPoint);
				if (globalGrid.ContainsKey(key))
				{
					// Key is New voxel is already in the Hashtable
				}
				else
				{
					globalGrid.Add(key, new Voxel(tempPoint, tempAttribute));
				}
			}
		}
	}
}
*/