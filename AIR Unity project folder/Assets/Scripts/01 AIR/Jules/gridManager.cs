﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class gridManager : MonoBehaviour {

	public GameObject voxel;

	int x;
	int y;
	int z;

	public int rows ;
	public int cols ;
	public int height;

	public int voxelCount;


	public TextAsset csvFile; // Reference of CSV file
	private char lineSeperater = '\n'; // It defines line seperate character
	private char fieldSeperator = ','; // It defines field seperate chracter

	public ArrayList attributes = new ArrayList ();

	public bool[] hasVisited;
	public int [] timesVisited;


	// Use this for initialization
	void Start () 
	{
		// running the function readData to textAsset csvFile, 
		//att is an empty array list that is populated with ReadData
		attributes = readData (csvFile);

		//data of visited cells in grid
		hasVisited = new bool[attributes.Count];
		timesVisited = new int[attributes.Count];

		//double array is an array of floats
		//debug stuff
		/*for (int i = 0; i < Att1.Count; i++) {
			string newline = "";
			for (int j = 0; j < ((double[])Att1[i]).Length; j++) {
				newline = newline + string.Format("{0} , {1} , {2}", ((double[])Att1[i])[j],i,j);
			}
			Debug.Log (newline);
		}
*/

		//tile generation
		for (x = 0; x < rows; x++) 
		{
			for (y = 0; y < cols; y++) 
			{
				for (z = 0; z < height; z++) 
				{

					//spawning tiles
					Vector3 pos = new Vector3 (x, z, y);
					Quaternion rot = Quaternion.identity;
					GameObject newObj = Instantiate (voxel, pos, rot) as GameObject;
					newObj.transform.parent = gameObject.transform;

					voxelCount++;
				}

			}
		}
	}

	// Read data from CSV file
	/// <summary>
	/// Reads the data.
	/// </summary>
	/// <returns>The data.</returns>
	/// <param name="csv">Csv.</param>

	//
	private ArrayList readData(TextAsset csv)
	{

		ArrayList outArrayList = new ArrayList();
		string[] records = csv.text.Split(lineSeperater);
		foreach (string record in records)
		{
			string[] fields = record.Split(fieldSeperator);

			double[] newAtt = new double[fields.Length];
			for (int i = 0; i < fields.Length; i++)
			{
				if (!double.TryParse(fields[i], out newAtt[i]))
				{
					// error handle
				}
			}
			outArrayList.Add(newAtt);
		}
		return outArrayList;
	}

	//function
	public int convertXYZtoI(float myX, float myZ, int gridWidth)
	{
		//int indice = 
		//return indicie
		return 0;
	}

	public int convertXYZtoI(Vector3 vecPos, int gridwidth) 
	{
		return convertXYZtoI (vecPos.x, vecPos.z, gridwidth);
	}
}