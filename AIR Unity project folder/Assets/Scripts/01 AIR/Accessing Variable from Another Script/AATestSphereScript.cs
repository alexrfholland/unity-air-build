﻿using UnityEngine;
using System.Collections;

public class AATestSphereScript : MonoBehaviour {

	//STEP 1: Script to access -- Script to access's variable in this script
	public AATestCubeScript ScriptFromCube;

	//STEP 2: the name of the variable from from other script in this script
	public int cubeCountfromScript;


	public GameObject cube;

	// Use this for initialization
	void Start () 
	{
		//STEP 3: shortcut: don't have to write cube.GetComponent<AATestCubeScript>(), can write ScriptFromCube instead
		ScriptFromCube = cube.GetComponent<AATestCubeScript> ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		//STEP4: Setting variable from other script
		ScriptFromCube.cubeCount += 1;

		//STEP 5: Update sphere's cubeCount
		cubeCountfromScript = cube.GetComponent<AATestCubeScript>().cubeCount;
		//cubeCountfromScript = ScriptFromCube.cubeCount;

	}
}
