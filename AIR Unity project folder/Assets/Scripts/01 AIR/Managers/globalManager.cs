﻿using UnityEngine;
using System.Collections;

public class globalManager : MonoBehaviour 
{

	public static float scalingFactor;
	public float setScalingFactor;

	public static float voxelScalingFactor;
	public float setVoxelScalingFactor;

	public static float combinedScalingFactor;

	// Use this for initialization

	void Awake()
	{
		scalingFactor = setScalingFactor;
		voxelScalingFactor = setVoxelScalingFactor;

		combinedScalingFactor = scalingFactor * voxelScalingFactor;
	}

}
