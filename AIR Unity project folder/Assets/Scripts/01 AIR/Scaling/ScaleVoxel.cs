﻿using UnityEngine;
using System.Collections;

public class ScaleVoxel : MonoBehaviour {

	// Use this for initialization
	public Vector3 size;

	void Start () 
	{
		size = new Vector3 (1 / globalManager.combinedScalingFactor, 1 / globalManager.combinedScalingFactor, 1 / globalManager.combinedScalingFactor);
		gameObject.transform.localScale = size;

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
