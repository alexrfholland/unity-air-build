﻿using UnityEngine;
using System.Collections;

public class RotateAndScale : MonoBehaviour {

	public GameObject character;
	public float offset;

	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{

		transform.position = new Vector3 (character.transform.position.x  * 100, character.transform.position.y  * 100, character.transform.position.z * 100 + offset);
		transform.rotation = character.transform.rotation;
	}
}
