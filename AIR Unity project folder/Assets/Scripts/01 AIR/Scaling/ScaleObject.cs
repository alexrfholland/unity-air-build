﻿using UnityEngine;
using System.Collections;

public class ScaleObject : MonoBehaviour 
{

	public Vector3 size;

	// Use this for initialization
	void Start ()
	{
		size = new Vector3 (1 / globalManager.scalingFactor, 1 / (globalManager.scalingFactor * 100), 1 / globalManager.scalingFactor);
		gameObject.transform.localScale = size;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
