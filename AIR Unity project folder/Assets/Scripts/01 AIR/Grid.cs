﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

	public GameObject tile;
	int x;
	int y;
	public int rows ;
	public int cols ;
	float extra;

	public float at1;
	public float at2;
	public float at3;

	// Use this for initialization
	void Start () 
	{
		for (x = 0; x < rows; x++) 
		{
			for (y = 0; y < cols; y++) 
			{

				//spawning tiles
				Vector3 pos = new Vector3 (x*3, 0, y*3);
				Quaternion rot = Quaternion.identity;
				GameObject newObj = Instantiate (tile, pos, rot) as GameObject;
				newObj.transform.parent = gameObject.transform;

			}
		}
			
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
