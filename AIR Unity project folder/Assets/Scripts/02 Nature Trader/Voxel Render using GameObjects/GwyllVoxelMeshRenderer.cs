﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;


public class GwyllVoxelMeshRenderer : MonoBehaviour 
{
	Renderer voxelRenderer;
	public Vector3 prevPosition;
	public GameObject manager;
	
	public VoxelGrid _CAGrid;

	void Start () 
	{
		voxelRenderer = gameObject.GetComponent<Renderer> ();
		voxelRenderer.enabled = false;

		//_CAGrid = manager.GetComponent<VoxelData> ().CA.grid;
	}

    // Update is called once per frame
    void Update()
    {
		if (VoxelData.CA.running && transform.position - prevPosition != new Vector3 (0,0,0) )
		{

				//float voxValue = VoxelData.CA.grid.getCell ((int)gameObject.transform.position.x * 2, (int)gameObject.transform.position.y * 2, (int)gameObject.transform.position.z * 2).Val;
			float voxValue = VoxelData.CA.grid.getCell ((int)gameObject.transform.position.x * 2, (int)gameObject.transform.position.y * 2, (int)gameObject.transform.position.z * 2).get();

			//float voxValue = _CAGrid.getCell ((int)gameObject.transform.position.x * 2, (int)gameObject.transform.position.y * 2, (int)gameObject.transform.position.z * 2).get();

			
			if (voxValue == 1)
			{
				voxelRenderer.enabled = true;
			}
			
			
			else
			{
				voxelRenderer.enabled = false;
			}
		}
    }
}
